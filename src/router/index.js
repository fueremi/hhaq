import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home'
import Login from '@/views/admin/Login'
import AdminHome from '@/views/admin/Home'
import MediaSosial from '@/views/admin/MediaSosial'
import Testimoni from '@/views/admin/Testimoni'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/admin/',
    name: 'AdminHome',
    component: AdminHome
  },
  {
    path: '/admin/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/admin/mediaSosial',
    name: 'MediaSosial',
    component: MediaSosial
  },
  {
    path: '/admin/testimoni',
    name: 'Testimoni',
    component: Testimoni
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
