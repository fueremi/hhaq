import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    testimoni: [],
    mediaSosial: [],
    paket: [],
    session: {
      message: '',
      status: false,
    },
  },
  mutations: {
    SET_TESTIMONI: (state, payload) => {
      state.testimoni = payload;
    },
    SET_MEDIASOSIAL: (state, payload) => {
      state.mediaSosial = payload;
    },
    SET_PAKET: (state, payload) => {
      state.paket = payload;
    },
    SET_SESSION: (state, payload) => {
      state.session = payload;
    },
  },
  actions: {
    async fetchTestimoni({ commit }) {
      const API_URL = "https://fueremi-hasura.herokuapp.com/v1/graphql";
      const API_HEADERS = {
        "Content-Type": "application/json",
        "x-hasura-admin-secret": "18032405",
      };
      const API_QUERY = `
      query MyQuery {
        hhaq_testimoni(limit: 10) {
          id
          image
          name
          rating
          desc
        }
      }
      `;
      const data = await axios.post(
        API_URL,
        { query: API_QUERY },
        { headers: API_HEADERS }
      );

      commit("SET_TESTIMONI", data.data.data.hhaq_testimoni);
    },
    async fetchMediaSosial({ commit }) {
      const API_URL = "https://fueremi-hasura.herokuapp.com/v1/graphql";
      const API_HEADERS = {
        "Content-Type": "application/json",
        "x-hasura-admin-secret": "18032405",
      };
      const API_QUERY = `
      query MyQuery {
        hhaq_media_sosial {
          id
          image
          name
          url
          url_phone
          username
        }
      }
      `;
      const data = await axios.post(
        API_URL,
        { query: API_QUERY },
        { headers: API_HEADERS }
      );

      commit("SET_MEDIASOSIAL", data.data.data.hhaq_media_sosial);
    },
    async fetchPaket({ commit }) {
      const API_URL = "https://fueremi-hasura.herokuapp.com/v1/graphql";
      const API_HEADERS = {
        "Content-Type": "application/json",
        "x-hasura-admin-secret": "18032405",
      };
      const API_QUERY = `
      query MyQuery {
        hhaq_paket {
          id
          name
          relasi_daftar_harga {
            harga
            deskripsi
            nama
            highlight
          }
          relasi_galeri {
            gambar
          }
        }
      }
      
      `;
      const data = await axios.post(
        API_URL,
        { query: API_QUERY },
        { headers: API_HEADERS }
      );

      commit("SET_PAKET", data.data.data.hhaq_paket);
    },
    async loginProcess({ commit }, params) {
      const API_URL = "https://fueremi-hasura.herokuapp.com/v1/graphql";
      const API_HEADERS = {
        "Content-Type": "application/json",
        "x-hasura-admin-secret": "18032405",
      };
      const API_QUERY_USERNAME = `
      query MyQuery {
        hhaq_user_admin(where: {username: {_eq: "${params.username}"}}) {
          id
          username
        }
      }          
      `;
      const API_QUERY_PASSWORD = `
      query MyQuery {
        hhaq_user_admin(where: {password: {_eq: "${params.password}"}}) {
          id
        }
      }          
      `;
      const data = await axios.post(
        API_URL,
        { query: API_QUERY_USERNAME },
        { headers: API_HEADERS }
      );

      if (data.data.data.hhaq_user_admin.length === 1) {
        const result = await axios.post(
          API_URL,
          { query: API_QUERY_PASSWORD },
          { headers: API_HEADERS }
        );

        if (result.data.data.hhaq_user_admin.length === 1) {
          const payload = {
            id: data.data.data.hhaq_user_admin[0].id,
            status: true,
            message: "Redirecting kamu ke halaman admin",
          };
          commit("SET_SESSION", payload);
        } else {
          const payload = {
            status: false,
            message: "Kombinasi username dan password salah",
          };
          commit("SET_SESSION", payload);
        }
      } else {
        const payload = {
          status: false,
          message: "Akun dengan username tersebut tidak ditemukan",
        };
        commit("SET_SESSION", payload);
      }
    },
  },
});
